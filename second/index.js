let pChart = document.getElementById('p-chart');
let bChart = document.getElementById('b-chart');
let gChart = document.getElementById('gen-chart');
let lChart = document.getElementById('l-chart');
let tGraph = document.getElementById('t-graph');

// var heightRatio = 0.3;


let xGrid=10;
let yGrid=10;
let cellSize=10;

let ctx = bChart.getContext('2d');
let ctx2 = pChart.getContext('2d');
let ctx3 = gChart.getContext('2d');
let ctx4 = lChart.getContext('2d');
let ctx5 = tGraph.getContext('2d');

let outPatients={
    Oct2019:1300,
    Nov2019:1600,
    Dec2019:500,
    Jan2020:1100,
    Feb2020:1300,
    Mar2020:1200
}
let inPatients={
    Oct2019:2800,
    Nov2019:3100,
    Dec2019:4300,
    Jan2020:2600,
    Feb2020:2800,
    Mar2020:3500
}
let addmitTime={
    am7:110,
    am8:105,
    am9:70,
    am10:130,
    am11:60,
    pm12:115
}

const entries1 = Object.entries(outPatients);
const entries2 = Object.entries(inPatients);
const entries3 = Object.entries(addmitTime);
function getWnH(){

    
    bChart.width=bChart.clientWidth;
    // bChart.height=180;
    bChart.height = bChart.width * .3;
    
    pChart.width=pChart.clientWidth;
    // pChart.height = 180;
    pChart.height = pChart.width * 0.7;
    
    gChart.width=gChart.clientWidth;
    // gChart.height= 190;
    gChart.height = gChart.width * .7;
    
    lChart.width=lChart.offsetWidth;
    lChart.height=lChart.width * 0.32;
    
    tGraph.width=tGraph.offsetWidth;
    tGraph.height=tGraph.width * .5;
    

    var imgPath = 'images/gender.png';
    var imgObj = new this.Image();
    imgObj.src=imgPath;
    imgObj.style.width='15px';
    imgObj.style.height='15px';
    imgObj.onload = function(){
        ctx3.drawImage(imgObj, 123, 73);
    }
    // var render = function(){
    //     bChart.width=document.documentElement.clientHeight * 0.5;
    //     bChart.height=document.documentElement.clientHeight * 0.5;
        
    //     pChart.width=document.documentElement.clientHeight * 0.5;
    //     pChart.height = document.documentElement.clientHeight * 0.5;
    // }
    // window.addEventListener("resize",render);
    // render();
    // drawGrid();
    drawAxis();
    drawChart();
    timeGraph();
    dashedLine();
    circleChart();
    genderCircleChart();
}

window.addEventListener('resize',getWnH);

function drawGrid(){
    // alert(bChart.height);
    ctx.beginPath();
    while(xGrid<bChart.height){
        ctx.moveTo(0,xGrid);
        ctx.lineTo(bChart.width,xGrid);
        xGrid+=10;
    }
    while(yGrid<bChart.width){
        ctx.moveTo(yGrid,0);
        ctx.lineTo(yGrid,bChart.height);
        yGrid+=10;
    }
    ctx.strokeStyle='grey';
    ctx.stroke();
}

function block(count){
    return count*10;
}

function drawAxis(){
    let yPlot=15;
    let num=0;
    
    // ctx.scale(.6,.6);
    ctx.beginPath();
    ctx.strokeStyle='black';
    
    ctx.moveTo(block(8),block(15));
    ctx.lineTo(block(58),block(15));
    
    ctx.moveTo(block(10),block(10));
    for(let i=1;i<=4;i++){
        ctx.font="13px Ariel";
        ctx.fillStyle="#a4a7b6";
        ctx.fillText(num,block(4),block(yPlot));
        yPlot-=4;
        num+=1500;
    }
    
    num=0;
    yPlot=15;
    ctx4.beginPath();
    ctx4.moveTo(block(7),block(yPlot));
    ctx4.lineTo(block(57),block(yPlot));
    
    ctx4.moveTo(block(10),block(20));
    for(let i=1;i<=4;i++){
        ctx4.fillStyle="#a4a7b6";
        ctx4.font="15px Ariel";
        ctx4.fillText(num,block(3),block(yPlot));
        yPlot-=4;
        num+=50;
    }
    ctx.stroke();
    ctx4.stroke();
}
function dashedLine(){
    let xDashed=11;
    ctx.beginPath();
    for(let i=1; i<=3; i++){
        ctx.setLineDash([5,5]);
        // ctx.lineDashOffset = 5;
        ctx.moveTo(block(8),block(xDashed));
        ctx.lineTo(block(58),block(xDashed));
        xDashed-=4;
    }

    xDashed=10.5;
    ctx4.beginPath();
    for(let i=1; i<=3; i++){
        ctx4.setLineDash([5,5]);
        // ctx.lineDashOffset = 5;
        ctx4.strokeStyle='grey';
        ctx4.moveTo(block(7),block(xDashed));
        ctx4.lineTo(block(57),block(xDashed));
        xDashed-=4;
    }
    ctx.stroke();
    ctx4.stroke();
}

function drawChart(){
    let yCount=15;
    ctx.beginPath();
    xPlot=11;
    for(const[monthYear,inNum] of entries1){
        var numInBlock= inNum/300;
        ctx.fillStyle='#3ad29f';
        ctx.fillRect(block(xPlot),block(yCount),block(1),block(-numInBlock));
        ctx.font="10px sans-serif";
        ctx.fillStyle='#a4a7b6';
        ctx.fillText(monthYear,block(xPlot),block(17));
        xPlot+=8;
    }
    xPlot=13
    for(const[monthYear,inNum] of entries2){
        var numInBlock= inNum/300;
        ctx.fillStyle='#7139e4';
        ctx.fillRect(block(xPlot),block(yCount),block(1),block(-numInBlock));
        xPlot+=8;
    }    
    ctx.stroke();

    xPlot=8;
    yCount=18;
    ctx4.beginPath();
    ctx4.moveTo(block(7),block(14));
    for(const[time,patientNum] of entries3){
        var pnumInBlock= patientNum/10;
        ctx4.strokeStyle='#edb296';
        ctx4.quadraticCurveTo(block(xPlot),block(yCount-(pnumInBlock/2)),block(++xPlot),block(yCount-pnumInBlock));
        ctx4.font="15px sans-serif";
        ctx4.fillStyle='#a4a7b6';
        ctx4.fillText(time,block(xPlot),block(18));
        xPlot+=7;
    }    
    ctx4.stroke();
}
function timeGraph(){
    xPlot=2;
    yCount=14;
    num=14;
    ctx5.beginPath();
    ctx5.moveTo(block(xPlot),block(11));
    for(var i=1; i<=6;i++){
        ctx5.strokeStyle="whitesmoke";
        ctx5.quadraticCurveTo(block(xPlot),block(yCount),block(xPlot+2),block(yCount-num+5));
        ctx5.font="10px sans-serif";
        ctx5.fillStyle="whitesmoke";
        ctx5.fillText(num,block(xPlot),block(yCount));
        xPlot+=4.5;
        num+=1
        
    }
    ctx5.stroke();
}
function circleChart(){
    ctx2.beginPath();
    ctx2.lineWidth=6;

    
    // ctx2.fillRect(block(1),block(1),block(3),block(2));
    // ctx2.fillStyle='black';
    // // cxt2.fill();
    // ctx2.fillText('28%',2,2);
    // ctx2.font='20px sans-serif';
    // cxt2.fillStyle='white';
    // ctz2.stroke();
    
    ctx2.beginPath();
    ctx2.arc(155,75,60,1.5*Math.PI,0.9*Math.PI,false);
    ctx2.strokeStyle='#7139e4';
    ctx2.stroke();
    ctx2.beginPath();
    ctx2.strokeStyle='#3ad29f';
    ctx2.arc(155,75,60,0.9*Math.PI,1.5*Math.PI);
    ctx2.font='70px Ariel';
    ctx2.fillStyle='#7139e4';
    ctx2.fillText('.',50,163);
    ctx2.font='15px sans-serif';
    ctx2.fillStyle='black';
    ctx2.fillText('Inpatients',70,165);

    ctx2.font='70px Ariel';
    ctx2.fillStyle='#3ad29f';
    ctx2.fillText('.',150,163);
    ctx2.font='15px sans-serif';
    ctx2.fillStyle='black';
    ctx2.fillText('Outpatients',170,165);
    ctx2.stroke();
}

function genderCircleChart(){
    ctx3.beginPath();
    ctx3.lineWidth=6;
    // var gradient=ctx3.createRadialGradient(20,50,5,100,110,110);
    // gradient.addColorStop('0.8','#7139e4');
    // gradient.addColorStop('0.5','#ff8851');

    // ctx3.strokeStyle=gradient;
    ctx3.arc(135,85,60,1.5*Math.PI,0.7*Math.PI);
    ctx3.strokeStyle='#7139e4';
    ctx3.stroke();
    
    ctx3.beginPath();
    ctx3.arc(135,85,60,0.7*Math.PI,1.5*Math.PI);
    ctx3.strokeStyle='#ff8851';

    ctx3.font='70px Ariel';
    ctx3.fillStyle='#ff8851';
    ctx3.fillText('.',50,172);
    ctx3.font='15px sans-serif';
    ctx3.fillStyle='black';
    ctx3.fillText('Female',70,174);

    ctx3.font='70px Ariel';
    ctx3.fillStyle='#7139e4';
    ctx3.fillText('.',150,172);
    ctx3.font='15px sans-serif';
    ctx3.fillStyle='black';
    ctx3.fillText('Male',170,174);
    ctx3.stroke();
}
window.onload = function() {
    var canvas = document.getElementById("p-chart");
    var ctx = canvas.getContext("2d");
    var img = document.getElementById("patient");
    ctx.drawImage(img, 135, 60);

    // var canvas1 = document.getElementById("gen-chart");
    // var ctx1 = canvas1.getContext("2d");
    
};
// var imgPath = 'images/gender.png';
// var imgObj = new this.Image();
// imgObj.src=imgPath;
// imgObj.style.width='15px';
// imgObj.style.height='15px';
// imgObj.onload = function(){
//     ctx3.drawImage(imgObj, 123, 110);

// }
getWnH();
// drawGrid();
// drawAxis();
// drawChart();
// timeGraph();
// dashedLine();
// circleChart();
// genderCircleChart();

function clickFun(){
    alert('working');
}